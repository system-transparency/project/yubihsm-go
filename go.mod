module git.glasklar.is/system-transparency/project/yubihsm-go

go 1.14

require (
	github.com/enceve/crypto v0.0.0-20160707101852-34d48bb93815
	golang.org/x/crypto v0.10.0
)
